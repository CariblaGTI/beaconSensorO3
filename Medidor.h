
// --------------------------------------------------------------
// --------------------------------------------------------------

// --------------------------------------------------------------
//
// Carlos Ibañez Lamas
// 2020-10-23
//
// --------------------------------------------------------------

#ifndef MEDIDOR_H_INCLUIDO
#define MEDIDOR_H_INCLUIDO

#define RX_PIN 15
#define TX_PIN 17 

// ------------------------------------------------------
// ------------------------------------------------------

class Medidor {

  private:

    int sensorData [11];

  public:

    int contadorO3 = 0;
    int contadorT = 0;
    int promediadoO3 = 0;
    int promediadoT = 0;

  // .....................................................
  // constructor
  // .....................................................
  
    Medidor(  ) {
    } // ()

  // .....................................................
  // .....................................................
  
    void iniciarMedidor() {
      Serial1.begin(9600);
      //leer11Datos(); 
    } // ()

  // .....................................................
  // .....................................................
  
    int medirO3() {
      int resO3;

      while (contadorO3 != 29){
          leer11Datos();
          promediadoO3 = promediadoO3 + sensorData[1];
          contadorO3++;
          delay(10);
        }

        resO3 = promediadoO3;
        Serial1.print("Promediado O3 : ");
        Serial1.print(resO3);
        Serial1.print('\r');
        promediadoO3 = 0;
        contadorO3 = 0;
        return resO3 / 30;
      
    } // ()

  // .....................................................
  // .....................................................
  
    int medirTemperatura() {
      int resT;
      
      while (contadorT != 29){
          leer11Datos();
          promediadoT = promediadoT + sensorData[2];
          contadorT++;
          delay(10);
        }
        
      resT = promediadoT;
      Serial1.print("Promediado T : ");
      Serial1.print(resT);
      Serial1.print('\r');
      promediadoT = 0;
      contadorT = 0;
      return resT/30;
           
    } // ()

  // .....................................................
  // .....................................................

    void leer11Datos(){
      Serial1.print('\r');
      int i = 0;
      for (int i =0; i<11; i++) {
        while(Serial1.available() == false) { 
          Serial.println("No se esta leyendo el sensor");
          delay(10); 
        }
       sensorData[i] = Serial1.parseInt();
      }  
    }
  
}; // class

#endif

// ------------------------------------------------------
// ------------------------------------------------------
