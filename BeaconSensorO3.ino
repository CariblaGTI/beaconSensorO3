// --------------------------------------------------------------
// --------------------------------------------------------------

// --------------------------------------------------------------
//
// Carlos Ibañez Lamas
// 2020-10-23
//
// --------------------------------------------------------------

// https://learn.sparkfun.com/tutorials/nrf52840-development-with-arduino-and-circuitpython

// https://stackoverflow.com/questions/29246805/can-an-ibeacon-have-a-data-payload

// --------------------------------------------------------------
// --------------------------------------------------------------

#include <bluefruit.h>

// Colision con biblioteca estandar, cuidado

#undef min 
#undef max 

// --------------------------------------------------------------
// --------------------------------------------------------------

#include "LED.h"
#include "Pantallita.h"

// --------------------------------------------------------------
// --------------------------------------------------------------

namespace Globales {
  
  LED elLED ( 7 );

  Pantallita laPantallita ( 9600 ); 
  
};

// --------------------------------------------------------------
// --------------------------------------------------------------

#include "EmisoraBLE.h"
#include "Publicador.h"
#include "Medidor.h"

// --------------------------------------------------------------
// --------------------------------------------------------------

namespace Globales {

  Publicador elPublicador;

  Medidor elMedidor;

}; // namespace

// --------------------------------------------------------------
// --------------------------------------------------------------

void inicializarPlaquita () {

  // de momento nada

} // ()

// --------------------------------------------------------------
// setup()
// --------------------------------------------------------------

void setup() {

  Globales::laPantallita.esperarDisponible();

  inicializarPlaquita();

  // Suspend Loop() to save power
  // suspendLoop();

  Globales::elPublicador.encenderEmisora();

  // Globales::elPublicador.laEmisora.pruebaEmision();

  Globales::elMedidor.iniciarMedidor();

  esperar( 1000 );

  Globales::laPantallita.escribir( "---- setup(): fin ---- \n " );

} // setup ()

// --------------------------------------------------------------
// --------------------------------------------------------------

inline void lucecitas() {
  
  using namespace Globales;

  elLED.brillar( 100 ); // 100 encendido
  esperar ( 400 ); //  100 apagado
  elLED.brillar( 100 ); // 100 encendido
  esperar ( 400 ); //  100 apagado
  Globales::elLED.brillar( 100 ); // 100 encendido
  esperar ( 400 ); //  100 apagado
  Globales::elLED.brillar( 1000 ); // 1000 encendido
  esperar ( 1000 ); //  100 apagado
  
} // ()

// --------------------------------------------------------------
// loop ()
// --------------------------------------------------------------

namespace Loop {
  uint8_t cont = 0;
};

// ..............................................................
// ..............................................................

void loop () {

  using namespace Loop;
  using namespace Globales;

  cont++;

  laPantallita.escribir( "\n---- loop(): empieza " );
  laPantallita.escribir( cont );
  laPantallita.escribir( "\n" );


  lucecitas();

  // Mido el valor de 03, haciendo un promedio de 30 mediciones diferentes 
  
  int valorO3 = elMedidor.medirO3();

  // Se comprueba que no es ningún valor de calibrado, sino un valor deseado

  if (valorO3 != NULL){
      laPantallita.escribir( "O3 : ");
      laPantallita.escribir( valorO3 );
      laPantallita.escribir( "\n" );

  // Se publica la medición para enviar al Beacon
  
    elPublicador.publicarO3( valorO3, cont, 1000 );
    
  }

  /* Mido el valor de Tª, haciendo un promedio de 30 mediciones diferentes 
  
  int valorTemperatura = elMedidor.medirTemperatura();

  // Se comprueba que no es ningún valor de calibrado, sino un valor deseado
  
  if (valorTemperatura != NULL){
      laPantallita.escribir( "Tª : ");
      laPantallita.escribir( valorTemperatura );
      laPantallita.escribir( "\n" );

  // Se publica la medición para enviar al Beacon
  
    elPublicador.publicarTemperatura( valorTemperatura, cont, 1000 );
    
  } */
  

  elPublicador.laEmisora.detenerAnuncio();
  
} // loop ()

// --------------------------------------------------------------
// --------------------------------------------------------------
