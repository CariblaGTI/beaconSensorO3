
// --------------------------------------------------------------
// --------------------------------------------------------------

// --------------------------------------------------------------
//
// Carlos Ibañez Lamas
// 2020-10-23
//
// --------------------------------------------------------------

#ifndef PUBLICADOR_H_INCLUIDO
#define PUBLICADOR_H_INCLUIDO

// --------------------------------------------------------------
// --------------------------------------------------------------

class Publicador {

  private:
  
    // He cambiiado la UUID para que al realizar un filtrado de Beacons por su UUID, me aparezca este Beacon (nombre provisional)
    uint8_t beaconUUID[16] = { 
    'G', 'T', 'I', '-', '3', 'A', '-', 'C', 
    'a', 'r', 'l', 'o', 's', '-', 'G', '5'
    };

  public:
    EmisoraBLE laEmisora {
    "GTI-3A-Carlos-G5", //  Nombre emisora (provisionalmente este nombre para diferenciarme de otros Beacons)
      0x004c, // FabricanteID (Apple)
      4 // txPower
      };
    
    const int RSSI = -53; // Por poner algo, de momento no lo uso
  
    // ............................................................
    // ............................................................
    
  public:
  
    // ............................................................
    // ............................................................
    
    enum MedicionesID  {
      O3 = 111, //Esta ID la he cambiado, para tambien diferenciarme de otros Beacons (lo usare en Android más adelante)
      TEMPERATURA = 12,
      RUIDO = 13
    };
  
    // ............................................................
    // ............................................................
    
    Publicador( ) {
      // ATENCION: no hacerlo aquí. (*this).laEmisora.encenderEmisora();
      // Pondremos un método para llamarlo desde el setup() más tarde
    } // ()
  
    // ............................................................
    // ............................................................
    
    void encenderEmisora() {
      (*this).laEmisora.encenderEmisora();
    } // ()
  
    // ............................................................
    // ............................................................
    
    void publicarO3( int16_t valorO3, uint8_t contador, long tiempoEspera ) {

      // 1. Empezamos anuncio
  
      uint16_t major = (MedicionesID::O3 << 8) + contador;
      (*this).laEmisora.emitirAnuncioIBeacon( (*this).beaconUUID, major, valorO3, // minor
                          (*this).RSSI // rssi
                      );
    
      // 2. Esperamos el tiempo que nos digan
  
      esperar( tiempoEspera );
  
      // 3. Paramos anuncio
  
      (*this).laEmisora.detenerAnuncio();
      
    } // ()
  
    // ............................................................
    // ............................................................
    
    void publicarTemperatura( int16_t valorTemperatura, uint8_t contador, long tiempoEspera ) {

    // 1. Empezamos anuncio
    
    uint16_t major = (MedicionesID::TEMPERATURA << 8) + contador;
    (*this).laEmisora.emitirAnuncioIBeacon( (*this).beaconUUID, major, valorTemperatura, // minor
                        (*this).RSSI // rssi
                    );

    // 2. Esperamos el tiempo que nos digan
                    
    esperar( tiempoEspera );

    // 3. Paramos anuncio
  
    (*this).laEmisora.detenerAnuncio();
    
    } // ()
  
}; // class

#endif

// ----------------------------------------------------------
// ----------------------------------------------------------

// --------------------------------------------------------------
// --------------------------------------------------------------
